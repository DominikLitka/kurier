﻿using Kurier5.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kurier5.ViewModels
{
    public class FinalVM
    {
        public IEnumerable<Sender> Senders { get; set; }
        public IEnumerable<Reciver> Recivers { get; set; }
        public IEnumerable<Pack> Packs { get; set; }
    }
}