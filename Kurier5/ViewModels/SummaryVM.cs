﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Kurier5.Models;

namespace Kurier5.ViewModels
{
    public class SummaryVM
    {
        public IEnumerable<Sender> Senders { get; set; }
        public IEnumerable<Reciver> Recivers { get; set; }

    }
}