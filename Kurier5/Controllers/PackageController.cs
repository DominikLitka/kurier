﻿using System.Web.Mvc;
using Kurier5.Models;

namespace Kurier5.Controllers
{
    public class PackageController : Controller
    {
        public readonly PostAppDatabaseEntities baza = new PostAppDatabaseEntities();
        // GET: Package
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        public ActionResult NewPack()
        {
            Pack package = new Pack();

            return View(package);
        }

        [HttpPost]
        public ActionResult NewPack(Pack package)
        {
            package.Dimensions = package.Height * package.Lenght * package.Width;
            baza.Packs.Add(package);
            baza.SaveChanges();

            return RedirectToAction("Final", "Final");
        }
      
    }
}