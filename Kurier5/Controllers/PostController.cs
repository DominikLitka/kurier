﻿using Kurier5.Models;
using System.Web.Mvc;

namespace Kurier5.Controllers
{
    public class PostController : Controller
    {
        public readonly PostAppDatabaseEntities postAppDatabaseEntities = new PostAppDatabaseEntities();
        // GET: Post
       
        public ActionResult Main()
        {
            return View();
        }
       
       
        [HttpGet]
        public ActionResult AddSender()
        {
            Sender sender = new Sender();
            return View(sender);

        }

        [HttpPost]
        public ActionResult AddSender(Sender sender)
        {
            if (!ModelState.IsValid)
            {
                return View("Error");
            }

            postAppDatabaseEntities.Senders.Add(sender);
            postAppDatabaseEntities.SaveChanges();

            return View("AddReciver");
        }

        public ActionResult DeleteSender(int id)
        {
            Sender sender = postAppDatabaseEntities.Senders.Find(id);

            postAppDatabaseEntities.Senders.Remove(sender);
            postAppDatabaseEntities.SaveChanges();

            return RedirectToAction("Index");
        }

       
        [HttpGet]
        public ActionResult AddReciver()
        {
            Reciver reciver = new Reciver();
            return View(reciver);

        }

        [HttpPost]
        public ActionResult AddReciver(Reciver reciver)
        {
            if (!ModelState.IsValid)
            {
                return View("Error");
            }
            
            postAppDatabaseEntities.Recivers.Add(reciver);
            postAppDatabaseEntities.SaveChanges();

            return RedirectToAction("Index","Summary");
        }
        public ActionResult DeleteReciver(int id)
        {
            Reciver reciver = postAppDatabaseEntities.Recivers.Find(id);
            

            postAppDatabaseEntities.Recivers.Remove(reciver);
            postAppDatabaseEntities.SaveChanges();

            return RedirectToAction("Index2");
        }

        [HttpGet]
        public ActionResult EditReciver(int id)
        {
            Reciver reciver = postAppDatabaseEntities.Recivers.Find(id);
            return View(reciver);
        }

        [HttpPost]
        public ActionResult EditReciver(Reciver reciver)
        {
            Reciver exsisingReciver= postAppDatabaseEntities.Recivers.Find(reciver.IdReciver);

            exsisingReciver.NameReciver = reciver.NameReciver;
            exsisingReciver.LastNameReciver = reciver.LastNameReciver;
            exsisingReciver.PhoneReciver = reciver.PhoneReciver;
            exsisingReciver.AddressReciver = reciver.AddressReciver;
            exsisingReciver.CityReciver = reciver.CityReciver;

            return RedirectToAction("Index");
        }
      



    }
}