﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Kurier5.Models;
using Kurier5.ViewModels;

namespace Kurier5.Controllers
{
    public class SummaryController : Controller
    {
        public readonly PostAppDatabaseEntities postAppDatabaseEntities = new PostAppDatabaseEntities();

        // GET: Summary
        [HttpGet]
        public ActionResult Index()
        {
            var sender = postAppDatabaseEntities.Senders;
            var reciver = postAppDatabaseEntities.Recivers;

            var SummaryVM = new SummaryVM
            {
                Senders = sender,
                Recivers = reciver,

            };

            return View(SummaryVM);
        }

        [HttpGet]
        public ActionResult EditSender(int id)
        {
            Sender sender = postAppDatabaseEntities.Senders.Find(id);

            return View(sender);
        }

        [HttpPost]
        public ActionResult EditSender(Sender sender)
        {
            Sender exsistingSender = postAppDatabaseEntities.Senders.Find(sender.IdSender);

            exsistingSender.LastNameSender = sender.LastNameSender;
            exsistingSender.NameSender = sender.NameSender;
            exsistingSender.CitySender = sender.CitySender;
            exsistingSender.AddressSender = sender.AddressSender;
            exsistingSender.PhoneSender = sender.PhoneSender;

            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult EditReciver(int id)
        {
            Reciver reciver = postAppDatabaseEntities.Recivers.Find(id);

            return View(reciver);
        }

        [HttpPost]
        public ActionResult EditReciver(Reciver reciver)
        {
            
            Reciver exsistingSender = postAppDatabaseEntities.Recivers.Find(reciver.IdReciver);

            exsistingSender.AddressReciver = reciver.AddressReciver;
            exsistingSender.CityReciver = reciver.CityReciver;
            exsistingSender.NameReciver = reciver.NameReciver;
            exsistingSender.LastNameReciver = reciver.LastNameReciver;
            exsistingSender.PhoneReciver = reciver.PhoneReciver;

            return RedirectToAction("Index");
        }

    }
}