﻿using Kurier5.Models;
using Kurier5.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kurier5.Controllers
{
    public class FinalController : Controller
    {
        public readonly PostAppDatabaseEntities baza = new PostAppDatabaseEntities();
        // GET: Final
        public ActionResult Final()
        {
            var sender = baza.Senders;
            var reciver = baza.Recivers;
            var pack = baza.Packs;

            var finalVM = new FinalVM
            {
                Senders = sender,
                Recivers = reciver,
                Packs = pack
            };

            return View(finalVM);
        }
        public ActionResult ThankYou()
        {
            return View();
        }
        public ActionResult Onas()
        {
            return View();
        }
        public ActionResult Soon()
        {
            return View();
        }
    }
}